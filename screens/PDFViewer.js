import React from 'react';
import { View, Text } from 'react-native';

import { StyleSheet } from 'react-native';

import { WebView } from 'react-native-webview';

class PDFViewer extends React.Component {

    // fileName = "";

    /* state = {
        fileName: ""
    } */

    constructor(props) {
        super(props)
        const name = this.props.route.params.fileName;
        console.log('name', name);
        this.setState({ fileName: name });
        // this.fileName = 'https://drive.google.com/viewerng/viewer?embedded=true&url=https://login-app.mark-43.net/auth-cliente/poliza/' + this.props.route.params.fileName;
        /* console.log(this.fileName) */
    }

    render() {
        const fileName = this.state;
        console.log(fileName);
        return (
            <View style={{
                flex: 1
            }}>
                <WebView
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    source={{ uri: "https://drive.google.com/viewerng/viewer?embedded=true&url=https://login-app.mark-43.net/auth-cliente/poliza/" + fileName }}></WebView>
            </View>
        );
    }
}

export default PDFViewer;