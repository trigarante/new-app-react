import React from "react";
import {
    ImageBackground,
    Image,
    StyleSheet,
    StatusBar,
    Dimensions
} from "react-native";
import {Block, Button, Text, theme} from "galio-framework";
import AsyncStorage from '@react-native-community/async-storage'

const {height, width} = Dimensions.get("screen");
const STORAGE_KEY = '@save_id'

import argonTheme from "../constants/Theme";
import Images from "../constants/Images";
import {Input} from "../components";
import axios from "axios";

class Onboarding extends React.Component {
    state = {
        email: "",
        password: "",
    };
    handleEmail = (text) => {
        this.setState({email: text});
    };
    handlePassword = (text) => {
        this.setState({password: text});
    };
    login = (email, pass) => {
        axios.post("https://login-app.mark-43.net/auth-cliente/login", {
            email: email,
            contrasenaApp: pass,

        })
            .then((response) => {
                AsyncStorage.setItem(STORAGE_KEY, response.data.id)
                console.log('ok')
                this.props.navigation.navigate('App')
            })
            .catch((err) => {
                throw err;
            });
    };

    render() {
        const {navigation} = this.props;

        return (
            <Block flex style={styles.container}>
                <StatusBar hidden/>
                <Block flex center>

                </Block>
                <Block center>
                    <Image source={Images.LogoOnboarding} style={styles.logo}/>
                </Block>
                <Block flex space="between" style={styles.padded}>
                    <Block flex space="around" style={{zIndex: 2}}>
                        <Block style={{paddingHorizontal: theme.SIZES.BASE}}>
                            <Input right placeholder="USUARIO" iconContent={<Block/>}
                                   onChangeText={this.handleEmail}/>
                        </Block>
                        <Block style={{paddingHorizontal: theme.SIZES.BASE}}>
                            <Input right placeholder="CONTRASEÑA" iconContent={<Block/>}
                                   secureTextEntry={true}
                                   onChangeText={this.handlePassword}/>
                        </Block>
                        <Block center>
                            <Button
                                style={styles.button}
                                color={argonTheme.COLORS.PRIMARY}
                                onPress={() => this.login(
                                    this.state.email,
                                    this.state.password,
                                )}
                                textStyle={{color: argonTheme.COLORS.WHITE}}
                            >
                                Iniciar Sesion
                            </Button>
                        </Block>
                    </Block>
                </Block>
            </Block>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.COLORS.WHITE
    },
    padded: {
        paddingHorizontal: theme.SIZES.BASE * 2,
        position: "relative",
        bottom: theme.SIZES.BASE,
        zIndex: 2,
    },
    button: {
        width: width - theme.SIZES.BASE * 4,
        height: theme.SIZES.BASE * 3,
        shadowRadius: 0,
        shadowOpacity: 0
    },
    logo: {
        zIndex: 2,
        position: 'relative',
        marginTop: '-50%'
    },
    title: {
        marginTop: '-5%'
    },
    subTitle: {
        marginTop: 20
    }
});

export default Onboarding;
