import React from 'react';
import { Dimensions, ScrollView, StyleSheet } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import { ButtonGroup, Card } from 'react-native-elements'
import axios from "axios";
import AsyncStorage from '@react-native-community/async-storage'
import { Image } from 'react-native';

const { width } = Dimensions.get('screen');
const STORAGE_KEY = '@save_id'


const component1 = () => <Text>Ver póliza</Text>
const component2 = () => <Text>Solicitudes</Text>
const component3 = () => <Text>Mi registro</Text>

class MisPolizas extends React.Component {
    constructor(props) {
        super(props);
        this.selectFunction = this.selectFunction.bind(this);
        this.state = {
            posts: [],
            id: "",
            selectedIndex: 0,
            archivo: ""
        }
    }


    async componentDidMount() {
        const userId = await AsyncStorage.getItem(STORAGE_KEY)
        axios.get('https://app.mark-43.net/mark43-service/v1/app-cliente/get-registro-cliente/' + userId)
            .then(res => {
                console.log(res)
                this.setState({
                    posts: res.data
                })
            })
    }

    selectFunction(selectedIndex, archivo) {
        switch (selectedIndex) {
            case 0:
                /* console.log(selectedIndex) */
                this.props.navigation.navigate('PDFViewer', {
                    fileName: archivo
                });
                console.log(archivo)
                break
            case 1:
                console.log(selectedIndex)
                break
            case 2:
                console.log(selectedIndex)
                break
        }

    }

    renderArticles = () => {
        const { posts } = this.state;
        const buttons = [{ element: component1 }, { element: component2 }, { element: component3 }];
        const { selectedIndex } = this.state;
        const { archivo } = this.state;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={styles.articles}>
                <Block flex>
                    {posts.map((item, index) => {
                        return (
                            <Card>
                                <Card.Title style={{ height: 70, padding: 0 }}>
                                    <Image style={styles.imageAlias} source={this.selectAliasImage(item.alias)}></Image>
                                </Card.Title>
                                <Card.Divider />

                                <Text style={{ marginBottom: 10 }}>
                                    <Text bold={true}>Número de Póliza:</Text> {item.poliza}
                                </Text>
                                <Text style={{ marginBottom: 10 }}>
                                    <Text bold={true}>Tipo:</Text> {item.tipoSubramo}
                                </Text>
                                <Text style={{ marginBottom: 10 }}>
                                    <Text bold={true}>Pago:</Text> {item.tipoPago}
                                </Text>
                                <Card.Divider />
                                <ButtonGroup
                                    onPress={(e) => this.selectFunction(e, item.archivo)}
                                    buttons={buttons}
                                    containerStyle={styles.btnGroup} />
                            </Card>
                        );
                    })}
                </Block>
            </ScrollView>
        )
    }

    selectAliasImage(alias) {
        const images = [
            { alias: "MIGO", image: require("../assets/imgs/logos/migo.jpg") },
            { alias: "SEGUROS AFIRME", image: require("../assets/imgs/logos/afirme.png") },
            { alias: "ANA SEGUROS", image: require("../assets/imgs/logos/ana.png") },
            { alias: "AXA", image: require("../assets/imgs/logos/axa.png") },
            { alias: "BANORTE", image: require("../assets/imgs/logos/banorte.png") },
            { alias: "GENERAL DE SEGUROS", image: require("../assets/imgs/logos/general.png") },
            { alias: "GNP", image: require("../assets/imgs/logos/gnp.png") },
            { alias: "HDI", image: require("../assets/imgs/logos/hdi.png") },
            { alias: "INBURSA", image: require("../assets/imgs/logos/inbursa.png") },
            { alias: "MAPFRE", image: require("../assets/imgs/logos/mapfre.png") },
            { alias: "MIGO", image: require("../assets/imgs/logos/migo.jpg") },
            { alias: "EL POTOSI", image: require("../assets/imgs/logos/elpotosi.png") },
            { alias: "QUALITAS", image: require("../assets/imgs/logos/qualitas.png") },
            { alias: "ZURA", image: require("../assets/imgs/logos/sura.png") },
            { alias: "ZURICH", image: require("../assets/imgs/logos/zurich.png") },
            { alias: "EL AGUILA", image: require("../assets/imgs/logos/elaguila.png") },
            { alias: "AIG", image: require("../assets/imgs/logos/aig.png") },
            { alias: "LA LATINO", image: require("../assets/imgs/logos/lalatino.png") }
        ];
        return images.find((e) => e.alias === alias).image;
    }


    render() {
        return (
            <Block flex center style={styles.home}>
                {this.renderArticles()}
            </Block>
        );
    }
}

const styles = StyleSheet.create({
    home: {
        width: width
    },
    articles: {
        width: width - theme.SIZES.BASE * 2,
        paddingVertical: theme.SIZES.BASE,
    },
    btnGroup: {
        height: 20,
        backgroundColor: 'white',
        color: '#000',
        alignItems: 'center',
        borderColor: '#fff'
    },
    imageAlias: {
        resizeMode: 'stretch',
        height: 50,
        width: 120
    }
});

export default MisPolizas;
